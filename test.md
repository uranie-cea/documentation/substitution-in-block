---
myst:
    substitutions:
        key: "I'm a **substitution**"
---
# Test

## Text

{{ key }}

## Code Block

{{
    "```\n" + key + "\n```"
}}

## Code Block + Environment

{{
    "```\n" + env.docname + "\n```"
}}
